open System
let tupleList = [1000,"M";900,"CM";500,"D";400,"CD";100,"C";90,"XC";50,"L";40,"XC";10,"X";9,"IX";5,"V";4,"IV";1,"I";0,""]
let rec get decimal index : int * string = 
    if decimal >= fst tupleList.[index] then
        tupleList.[index]
    else
        get decimal (index + 1)
let rec generate decimal numeral : string = 
    if decimal = 0 then 
        numeral
    else
        generate (decimal - fst (get decimal 0)) (numeral + snd (get decimal 0))
let toint input : int = 
    int input
[<EntryPoint>]
let main (argv :string[]) = 
    let mutable output : string = ""
    for input in argv do
        output <- generate (toint input) ""
        printfn "%A" output
    0